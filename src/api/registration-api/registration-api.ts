import { api } from "../../configs/axios-config"

export const registrationApi = {
  register: async (data: RegistrationDto): Promise<void> => {
    return await api.post("/register", data)
  },
}
