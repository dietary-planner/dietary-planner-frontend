interface RegistrationDto {
  nickname: string
  email: string
  password: string
}
