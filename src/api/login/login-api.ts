import { api } from "../../configs/axios-config"
import { LoginRequestDto, ApiLoginResponseDto, LoginResponseDto } from "./login-dtos"

export const loginApi = {
  login: async (request: LoginRequestDto): Promise<LoginResponseDto> => {
    const token = btoa(`${request.login}:${request.password}`)
    const authorization = `Basic ${token}`

    return await api
      .post<ApiLoginResponseDto>("/login", null, {
        headers: { Authorization: authorization },
      })
      .then(() => ({ authCookie: authorization }))
  },
}
