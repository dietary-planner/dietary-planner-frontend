export interface ApiLoginResponseDto {
  result: string
}

export interface LoginResponseDto {
  authCookie: string
}

export interface LoginRequestDto {
  login: string
  password: string
}
