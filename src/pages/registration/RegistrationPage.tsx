import { IonContent, IonPage, IonRouterLink, useIonRouter } from "@ionic/react"
import { Avatar, Box, Button, Container, Grid, Link, Typography } from "@mui/material"
import LockOutlinedIcon from "@mui/icons-material/LockOutlined"
import { Form, Formik } from "formik"
import AppInput from "../../components/AppInput/AppInput"
import PasswordInput from "../../components/AppInput/PasswordInput"
import * as Yup from "yup"
import { registrationApi } from "../../api/registration-api/registration-api"
import { useAuthContext } from "../../context/AuthContext"
import { useEffect } from "react"

interface RegistrationFormFields {
  nickname: string
  email: string
  password: string
}

const initialValues: RegistrationFormFields = {
  nickname: "",
  email: "",
  password: "",
}

const validationSchema = Yup.object({
  nickname: Yup.string().required("Nickname required"),
  email: Yup.string().email("Invalid email").required("Email required"),
  password: Yup.string().required("Password required"),
})

export default function RegistrationPage() {
  const { isAuthenticated } = useAuthContext()
  const router = useIonRouter()

  useEffect(() => {
    // TODO: useEffect runs after component render, so registration form is visible for a moment
    if (isAuthenticated) {
      router.push("/home", "forward", "replace")
    }
  }, [])

  const onLoginSubmit = async (data: RegistrationFormFields) => {
    try {
      await registrationApi.register({ ...data })
      router.push("/login")
    } catch (e) {
      // TODO: show notification? or add some error text below the form?
      console.error(e)
    }
  }

  return (
    <IonPage>
      <IonContent fullscreen>
        <Container component="main" maxWidth="xs">
          <Box
            sx={{
              marginTop: 8,
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <Avatar sx={{ margin: 1 }}>
              <LockOutlinedIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
              Sign in
            </Typography>
            <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={onLoginSubmit}>
              {(formikProps) => (
                <Box component={Form} sx={{ marginTop: 2 }}>
                  <AppInput
                    sx={{ marginTop: 1 }}
                    name={"nickname"}
                    label={"Nickname *"}
                    placeholder={"Enter your nickname"}
                    type={"text"}
                    touched={formikProps.touched.nickname}
                    error={formikProps.errors.nickname}
                  />
                  <AppInput
                    sx={{ marginTop: 1 }}
                    name={"email"}
                    label={"Email Address *"}
                    placeholder={"Enter your email"}
                    type={"email"}
                    touched={formikProps.touched.email}
                    error={formikProps.errors.email}
                  />
                  <PasswordInput
                    sx={{ marginTop: 1 }}
                    name={"password"}
                    label={"Password *"}
                    placeholder={"Enter your password"}
                    touched={formikProps.touched.password}
                    error={formikProps.errors.password}
                  />
                  <Button type="submit" variant="contained" fullWidth sx={{ marginTop: 1, marginBottom: 2 }}>
                    Sign In
                  </Button>
                  <Grid container justifyContent="flex-end">
                    <Grid item>
                      <Link variant="body2" component={IonRouterLink} routerLink={"/login"}>
                        Already have an account? Sign in
                      </Link>
                    </Grid>
                  </Grid>
                </Box>
              )}
            </Formik>
          </Box>
        </Container>
      </IonContent>
    </IonPage>
  )
};
