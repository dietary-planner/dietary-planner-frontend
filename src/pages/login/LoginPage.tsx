import { IonContent, IonPage, IonRouterLink, useIonRouter } from "@ionic/react"
import { loginApi } from "../../api/login/login-api"
import { useAuthContext } from "../../context/AuthContext"
import { Form, Formik } from "formik"
import * as Yup from "yup"
import AppInput from "../../components/AppInput/AppInput"
import { Avatar, Box, Button, Container, Grid, Link, Typography } from "@mui/material"
import { useEffect } from "react"
import LockOutlinedIcon from "@mui/icons-material/LockOutlined"
import PasswordInput from "../../components/AppInput/PasswordInput"

interface LoginFormFields {
  email: string
  password: string
}

const initialValues: LoginFormFields = {
  email: "",
  password: "",
}

const validationSchema = Yup.object({
  email: Yup.string().email("Invalid email").required("Email required"),
  password: Yup.string().required("Password required"),
})

export default function LoginPage() {
  const { isAuthenticated, login } = useAuthContext()
  const router = useIonRouter()

  useEffect(() => {
    // TODO: useEffect runs after component render, so login form is visible for a moment
    if (isAuthenticated) {
      router.push("/home", "forward", "replace")
    }
  }, [])

  const onLoginSubmit = async (data: LoginFormFields) => {
    try {
      const response = await loginApi.login({ login: data.email, password: data.password })
      login(response.authCookie)
    } catch (e) {
      // TODO: show notification? or add some error text below the form?
      console.error(e)
    }
  }

  return (
    <IonPage>
      <IonContent fullscreen>
        <Container component="main" maxWidth="xs">
          <Box
            sx={{
              marginTop: 8,
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <Avatar sx={{ margin: 1 }}>
              <LockOutlinedIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
              Sign in
            </Typography>
            <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={onLoginSubmit}>
              {(formikProps) => (
                <Box component={Form} sx={{ marginTop: 2 }}>
                  <AppInput
                    sx={{ marginTop: 1 }}
                    name={"email"}
                    label={"Email Address *"}
                    placeholder={"Enter your email"}
                    type={"email"}
                    touched={formikProps.touched.email}
                    error={formikProps.errors.email}
                  />
                  <PasswordInput
                    sx={{ marginTop: 1 }}
                    name={"password"}
                    label={"Password *"}
                    placeholder={"Enter your password"}
                    touched={formikProps.touched.password}
                    error={formikProps.errors.password}
                  />
                  <Button type="submit" variant="contained" fullWidth sx={{ marginTop: 1, marginBottom: 2 }}>
                    Sign In
                  </Button>
                  <Grid container>
                    <Grid item xs>
                      <Link variant="body2" component={IonRouterLink} routerLink={"/reset-password"}>
                        Forgot password?
                      </Link>
                    </Grid>
                    <Grid item>
                      <Link variant="body2" component={IonRouterLink} routerLink={"/register"}>
                        Don't have an account? Sign Up
                      </Link>
                    </Grid>
                  </Grid>
                </Box>
              )}
            </Formik>
          </Box>
        </Container>
      </IonContent>
    </IonPage>
  )
}
