import axios, { AxiosError } from "axios"
import Cookies from "js-cookie"
import { HttpHeaders } from "../constants/HttpHeaders"

export const api = axios.create({
  baseURL: "http://localhost:8080/api",
})

// defining a custom error handler for all APIs
const errorHandler = (error: AxiosError) => {
  const statusCode = error.response?.status

  // logging only errors that are not 401
  if (statusCode && statusCode !== 401) {
    console.error(error)
  }

  return Promise.reject(error)
}

// registering the custom error handler to the
// "api" axios instance
api.interceptors.response.use(undefined, (error) => {
  return errorHandler(error)
})

api.interceptors.request.use(async (config) => {
  const headerCookie = Cookies.get(HttpHeaders.Authorization)
  if (headerCookie) {
    config.headers.Authorization = headerCookie
  }
  return config
})
