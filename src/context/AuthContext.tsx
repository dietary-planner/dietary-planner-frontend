import { createContext, ReactNode, useContext, useEffect, useState } from "react"
import Cookies from "js-cookie"
import { useIonRouter } from "@ionic/react"
import { HttpHeaders } from "../constants/HttpHeaders"

interface AuthContextType {
  isAuthenticated: boolean
  login: (authCookie: string) => void
  logout: () => void
}

const AuthContext = createContext<AuthContextType>(null!)

export const useAuthContext = () => {
  if (!AuthContext) {
    throw new Error("useAuth must be used within an AuthProvider")
  }
  return useContext(AuthContext)
}

interface AuthProviderProps {
  children: ReactNode
}

const avoidRedirectPaths = ["/login", "/register"]

export function AuthProvider({ children }: AuthProviderProps) {
  const router = useIonRouter()
  const [redirectTo, setRedirectAfterLoginTo] = useState("/home")
  const [isAuthenticated, setAuthenticated] = useState(!!Cookies.get(HttpHeaders.Authorization))

  useEffect(() => {
    const pathname = router.routeInfo.lastPathname
    if (pathname && !avoidRedirectPaths.includes(pathname)) {
      setRedirectAfterLoginTo(pathname)
    }
  }, [router.routeInfo.lastPathname])

  const login = (authCookie: string) => {
    Cookies.set(HttpHeaders.Authorization, authCookie)
    setAuthenticated(true)
    router.push(redirectTo, "forward", "replace")
  }

  const logout = () => {
    Cookies.remove(HttpHeaders.Authorization)
    setAuthenticated(false)
    router.push("/login")
  }

  return <AuthContext.Provider value={{ isAuthenticated, login, logout }}>{children}</AuthContext.Provider>
}
