import { TextFieldTypes } from "@ionic/core"
import { Field } from "formik"
import { TextField } from "@mui/material"
import { ReactNode } from "react"
import { SxProps } from "@mui/system"
import { Theme } from "@mui/material/styles"

export interface AppInputProps {
  sx?: SxProps<Theme>
  name: string
  label: string
  placeholder?: string
  touched?: boolean
  error?: string
  type: TextFieldTypes
  endAdornment?: ReactNode
}

export default function AppInput({
  sx,
  name,
  label,
  placeholder,
  type = "text",
  touched,
  error,
  endAdornment,
}: AppInputProps) {
  return (
    <Field
      as={TextField}
      sx={sx}
      id={name}
      name={name}
      type={type}
      label={label}
      placeholder={placeholder}
      error={touched && !!error}
      helperText={touched && error ? error : " "}
      fullWidth
      InputProps={{
        endAdornment: endAdornment,
      }}
    />
  )
}
