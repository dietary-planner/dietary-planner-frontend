import { useState } from "react"
import { IconButton, InputAdornment } from "@mui/material"
import { Visibility, VisibilityOff } from "@mui/icons-material"
import AppInput, { AppInputProps } from "./AppInput"

export type PasswordInputProps = Omit<AppInputProps, "type" | "endAdornment">

export default function PasswordInput(props: PasswordInputProps) {
  const [showPassword, setShowPassword] = useState(false)
  function toggleShowPassword() {
    setShowPassword((prevShowPassword) => !prevShowPassword)
  }

  return (
    <AppInput
      {...props}
      type={showPassword ? "text" : "password"}
      endAdornment={
        <InputAdornment position="end">
          <IconButton onClick={toggleShowPassword}>
            {showPassword ? <VisibilityOff /> : <Visibility />}
          </IconButton>
        </InputAdornment>
      }
    />
  );
};
