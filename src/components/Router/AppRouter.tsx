import { IonRouterOutlet } from "@ionic/react"
import { Redirect, Route } from "react-router-dom"
import Home from "../../pages/Home"
import LoginPage from "../../pages/login/LoginPage"
import { IonReactRouter } from "@ionic/react-router"
import React from "react"
import ProtectedRoute from "./ProtectedRoute"
import { AuthProvider } from "../../context/AuthContext"

export default function AppRouter() {
  return (
    <IonReactRouter>
      <AuthProvider>
        <IonRouterOutlet>
          <Route exact path="/login">
            <LoginPage />
          </Route>
          <Route exact path="/register">
            <RegistrationPage />
          </Route>
          <ProtectedRoute exact path="/home">
            <Home />
          </ProtectedRoute>
          <ProtectedRoute exact path="/">
            <Redirect to="/home" />
          </ProtectedRoute>
        </IonRouterOutlet>
      </AuthProvider>
    </IonReactRouter>
  )
}
