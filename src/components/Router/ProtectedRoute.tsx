import { Redirect, Route } from "react-router-dom"
import { ReactNode } from "react"
import { useAuthContext } from "../../context/AuthContext"

interface ProtectedRouteProps {
  children: ReactNode
  path: string
  exact?: boolean
}

export default function ProtectedRoute({ children, ...rest }: ProtectedRouteProps) {
  const { isAuthenticated } = useAuthContext()
  return <Route {...rest}>{isAuthenticated ? children : <Redirect to="/login" />}</Route>
}
